'use strict';

const nconf   = require('nconf');
const AppServer = require('./bin/app/server');
const globalConfig = require('./bin/infra/configs/global_config');
const mysql = require('./bin/helpers/databases/mysql/connection');
const configs = require('./bin/infra/configs/config');
const logger = require('./bin/helpers/utils/logger')
configs.initEnvironments(nconf);
const appServer = new AppServer();
const port = process.env.port || nconf.get('PORT') || 1337;
appServer.server.listen(port, () => {
    mysql.init(globalConfig.DB_URL());
    logger.log('app-listen', `${appServer.server.listen} started, listening at ${appServer.server.url}`, `initate application`)
});