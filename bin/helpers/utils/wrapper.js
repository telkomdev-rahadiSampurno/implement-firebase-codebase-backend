'use strict';

const data = (data, description = null, code = 200) => {
    return { err: null, data: data, code: code, message: description };
}

const data1 = (data, description = null, content, code = 0) => {
    return { err: null, data: data, code: code, message: description, content: content };
}

const data2 = (code, content = null, totalItems = 0, message) => {
    return { err: null, code: code, content: content, totalItems: totalItems, message: message };
}

const data3 = (code, content, totalItems = 0, message) => {
    return { err: null, code: code, content: content, totalItems: totalItems, message: message };
}

const paginationData = (data, meta, description = null, code = 200) => {
    return { err: null, data: data, meta: meta, code: code, message: description };
}

const error = (err, description, code = 500) => {
    return { err: err, data: null, code: code, message: description };
}

const tMoneyError = (err, resultCode = 500, resultDesc, message, friendlyMessage, status) => {
    return { err: err, resultCode: resultCode, resultDesc: resultDesc, message: message, friendlyMessage: friendlyMessage, status: status, content: null };
}

const response = (res, type, result, message = null, code = null) => {
    if (message) {
        result.message = message;
    }
    if (code) {
        result.code = code;
    }
    let status = false;
    switch (type) {
        case 'fail':
            status = false;
            break;
        case 'success':
            status = true;
            break;
    }
    res.send(
        {
            success: status,
            data: result.data,
            message: result.message,
            code: result.code
        }
    );
}

const responseTmoney2 = (res, result) => {
    res.send(
        {
            resultCode: result.code,
            resultDesc: result.data,
            message: result.message,
            content: result.content
        }
    );
}

const paginationResponse = (res, type, result, message = null, code = null) => {
    if (message) {
        result.message = message;
    }
    if (code) {
        result.code = code;
    }
    let status = `error`;
    switch (type) {
        case 'fail':
            status = 'fail';
            break;
        case 'success':
            status = 'success';
            break;
    }
    res.send(
        {
            status: status,
            data: result.data,
            meta: result.meta,
            code: result.code,
            message: result.message
        }
    );
}

const tmoneyResponse2 = (err, resultCode, resultDesc, message, friendlyMessage, timeStamp) => {
    return { err: err, resultCode: resultCode, resultDesc: resultDesc, message: message, friendlyMessage: friendlyMessage, timeStamp: timeStamp };
}

const responseTmoney = (resultCode, resultDesc, message) => {
    return { err: null, resultCode: resultCode, resultDesc: resultDesc, message: message };
}

const responseTmoneyOtp = (resultCode, resultDesc, content) => {
    return { err: null, resultCode: resultCode, resultDesc: resultDesc, content: content };
}

const responseQren = (res, result) => {
    res.send(
        {
            resultCode: result.resultCode,
            resultDesc: result.resultDesc,
            message: result.message
        }
    );
}

const get = (res, code, content, total, message) => {
    res.send(
        {
            code: code,
            content: content,
            totalItems: total,
            message: message
        }
    );
}

const responseQrenOtp = (res, result) => {
    res.send(
        {
            resultCode: result.resultCode,
            resultDesc: result.resultDesc,
            content: result.content
        }
    );
}

const responseQrenTime = (res, result) => {
    res.send(
        {
            resultCode: result.resultCode,
            resultDesc: result.resultDesc,
            message: result.message,
            friendlyMessage: result.friendlyMessage,
            timeStamp: result.timeStamp
        }
    );
}

const tmoneyResponse = (err, resultCode, resultDesc, message, friendlyMessage, timeStamp) => {
    return { err: err, resultCode: resultCode, resultDesc: resultDesc, message: message, friendlyMessage: friendlyMessage, timeStamp: timeStamp };

}

const tmoneyResponseError = (res, resultCode, resultDesc, message, friendlyMessage, status) => {
    res.send(
        {
            resultCode: resultCode,
            resultDesc: resultDesc,
            message: message,
            friendlyMessage: friendlyMessage,
            status: status,
            content: {}
        }
    );
}

const errorTmoney = (err, result) => {
    return { err: err, resultCode: result.resultCode, resultDesc: result.resultDesc, message: result.message, friendlyMessage: result.friendlyMessage, timeStamp: result.timeStamp };
}

module.exports = {
    data,
    get,
    data1,
    data2,
    data3,
    paginationData,
    error,
    response,
    paginationResponse,
    tmoneyResponse,
    tmoneyResponse2,
    responseTmoney,
    responseTmoney2,
    responseQren,
    responseQrenTime,
    errorTmoney,
    responseTmoneyOtp,
    responseQrenOtp,
    tMoneyError,
    tmoneyResponseError
}