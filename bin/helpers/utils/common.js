'use strict';

const getLastFromURL = async (url) => {
    let name = decodeURI(url).split('/').pop();
    name = name.replace(/(\r\n|\n|\r)/gm, "");
    return String(name);
}

const convertToJSON = async (url) => {
    let hash;
    let myJson = {};
    let hashes = url.slice(url.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        myJson[hash[0]] = hash[1];
    }
    return myJson;
}

const isPostiveNumber = (parameter) => {
    let res = false;
    if (parameter != null && !isNaN(parameter) && parameter > 0) {
        res = true;
    }
    return res;
}

module.exports = {
    getLastFromURL: getLastFromURL,
    convertToJSON: convertToJSON,
    isPostiveNumber: isPostiveNumber
}