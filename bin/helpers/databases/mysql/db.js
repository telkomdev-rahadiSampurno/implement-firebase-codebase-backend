'use strict';

const mysql = require('mysql');
const wrapper = require('../../utils/wrapper');
const connect = require('./connection');

class DB {

    done(connection) {
        connection.release();
    }

    async findAlls(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async insertOne(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async updateData(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async deleteData(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async countData(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async findData(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async insert(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result.data);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async update(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result.data);
        }).catch(err => {
            return err;
        });
        return result;
    }

    async delete(statement) {
        const self = this;
        const pool = await connect.getConn();
        const recordset = () => {
            return new Promise((resolve, reject) => {
                pool.getConnection((err, connection) => {
                    if (err) {
                        let errorMessage;
                        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
                            errorMessage = 'Database connection was closed.';
                        }
                        if (err.code === 'ER_CON_COUNT_ERROR') {
                            errorMessage = 'Database has too many connections.';
                        }
                        if (err.code === 'ECONNREFUSED') {
                            errorMessage = 'Database connection was refused.';
                        }
                        self.done(connection);
                        return reject(wrapper.error(err.code, errorMessage, 503));
                    }
                    else {
                        connection.query(statement, function (err, result) {
                            if (err) {
                                self.done(connection);
                                return reject(wrapper.error(err.code, errorMessage, 503));
                            }
                            else {
                                self.done(connection);
                                return resolve(wrapper.data(JSON.stringify(result)));
                            }
                        });
                    }
                });
            });
        }
        const result = await recordset().then(result => {
            return wrapper.data(result.data);
        }).catch(err => {
            return err;
        });
        return result;
    }

}

module.exports = DB;