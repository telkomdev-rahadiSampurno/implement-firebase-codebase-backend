'use strict';

const redis = require("redis");
const bluebird = require("bluebird");
const logger = require('../../utils/logger');
const config = require('../../../infra/configs/global_config');

class DB {
    constructor(redisHost, redisPort, redisAuth) {
        this.redisHost = redisHost;
        this.redisPort = redisPort;
        this.redisAuth = redisAuth;
    }

    async init() {
        const client = redis.createClient(config.getRedisPort(), config.getRedisHost(), config.getRedisAuthentication());
        bluebird.promisifyAll(redis.RedisClient.prototype);
        bluebird.promisifyAll(redis.Multi.prototype);
        client.on("error", function (err) {
            logger.log('Redis Client', err, 'Redis Connection');
        });
        this.client = client;
    }

    async setValue(key, value) {
        const result = this.client.setAsync(key, value);
        return result;
    }

    async getValue(key) {
        const result = this.client.getAsync(key);
        return result;
    }
}

module.exports = DB;