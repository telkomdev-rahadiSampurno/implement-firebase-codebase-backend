'use strict';

const restify = require('restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper'); 
const firebase = require('../module/firebase/handlers/api_handler');

let AppServer = function () {
    this.server = restify.createServer({
        name: project.name + '-server',
        version: project.version
    });

    this.server.serverKey = '';
    this.server.use(restify.acceptParser(this.server.acceptable));
    this.server.use(restify.queryParser());
    this.server.use(restify.bodyParser());
    this.server.use(restify.authorizationParser());

    //required for basic auth
    this.server.use(basicAuth.init());
    this.server.get('/', (req,res) => {
        wrapper.response(res, `success`, wrapper.data(`Simple Firebase`), `This service is running properly`);
    });

    this.server.post('/sharing/sendToDevice',basicAuth.isAuthenticated, firebase.sendToDevice)
    this.server.post('/sharing/sendToTopic',basicAuth.isAuthenticated, firebase.sendToTopic)
    this.server.post('/sharing/sendToGroup',basicAuth.isAuthenticated, firebase.sendToGroup)
    this.server.post('/sharing/sendMulticast',basicAuth.isAuthenticated, firebase.sendMulticast
    )    
    

};

module.exports = AppServer;