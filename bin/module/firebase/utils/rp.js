const rp = require('request-promise');

class firebase {

  async createGroup(data) { 
    let option = {
      uri: 'https://fcm.googleapis.com/fcm/notification',
      headers: {
        'Authorization': 'AAAAWnE0Y9A:APA91bHHoB7FUEEvV2s8oWtrCBJ2I-nSkZHWJuDxwPVsWtASxWgKAEO7hKPR7DCQ6KXKJ5-1gGDtXyCyIh2E7jWgYsNj_7T61jX4vB4vR-C-4Wgy__iCTUrZ1h0dZ_YrLb0PnrMqhQ_G',
        'project_id': '436859265429',
        'Content-Type': 'application/json' 
      },
      body: {
        'operation': 'create',
        'notification_key_name': data.notification_key_name,
        'registration_ids': ["dZCe_I40Z0U:APA91bGlMrfN6v8jTkzamDreOGOxRXt9yzql8IZNIi_xZIy5_TpQvTgnKEPA7tPLmFcTQfLOTFZi8RkL1x7P7s5NFyUtMemLvqTi4FbycuLAIR55A-LPE8805pOITLLjWoe0isiUvKZP",
          "cF6bIOL8LgY:APA91bEU42XteYVuOqDkoSde3bI_1joOvsCtIdYuNnPJzZPQfilVHU-YGr2rR9lJtvhENW3980XflmeaAA9YlSVMz-Mnk0xoBqnWDUBmL5trCMvzhil8XeUwdYCRc_-RZMSi42vGsp0n"]
      }

    }
    try { 
      let create = await rp.post(option);
      let params = {
        'status': true,
        'data': JSON.parse(create),
        'dataForm': options.body,
        'messages': 'Success Registration'
      };
      return params;
    } catch (error) {
      return error;
    }

  }

}

module.exports = firebase;