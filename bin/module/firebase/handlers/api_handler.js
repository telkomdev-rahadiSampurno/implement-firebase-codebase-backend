

const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');

const sendToTopic = async (req, res) => {
  const sendtotopic = async () => {
    return await commandHandler.sendToTopic(req.params);
  };
  const sendResponse = (result) => {
    (result.err) ? wrapper.get(res, result.code, result.content, result.total, result.message) : 
      wrapper.get(res, result.code, result.content, result.totalItems, result.message);
  };
  sendResponse(await sendtotopic());
}; 

const sendToDevice = async (req, res) => {
  const sendtodevice = async () => {
    return await commandHandler.sendToDevice(req.params);
  };
  const sendResponse = (result) => {
    (result.err) ? wrapper.get(res, result.code, result.content, result.total, result.message) : 
      wrapper.get(res, result.code, result.content, result.totalItems, result.message);
  };
  sendResponse(await sendtodevice());
}; 
const sendToGroup = async (req, res) => {
  const sendtogroup = async () => {
    return await commandHandler.sendToGroup(req.params);
  };
  const sendResponse = (result) => {
    (result.err) ? wrapper.get(res, result.code, result.content, result.total, result.message) : 
      wrapper.get(res, result.code, result.content, result.totalItems, result.message);
  };
  sendResponse(await sendtogroup());
}; 

const sendMulticast = async (req, res) => {
  const sendmulticast = async () => {
    return await commandHandler.sendMulticast(req.params);
  };
  const sendResponse = (result) => {
    console.log(result,"ASDASDASD");
    (result.err) ? wrapper.get(res, result.code, result.content, result.total, result.message) : 
      wrapper.get(res, result.code, result.content, result.totalItems, result.message);
  };
  sendResponse(await sendmulticast());
}; 

module.exports = {
  sendToTopic,
  sendToDevice,
  sendToGroup,
  sendMulticast
};
