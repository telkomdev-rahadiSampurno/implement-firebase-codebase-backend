
const firebase = require('./domain');

const sendToTopic = async (data) => {
  const sendtotopic = async () => {
    const reg = new firebase();
    const result = await reg.sendToTopic(data);
    return result;
  };
  const response = await sendtotopic();
  console.log("response comand_halder", response);
  return response;
}; 

const sendToDevice = async (data) => {
  const sendtodevice = async () => {
    const reg = new firebase();
    const result = await reg.sendToDevice(data);
    return result;
  };
  const response = await sendtodevice();
  console.log("response comand_hald22er", response);
  return response;
}; 

const sendToGroup = async (data) => {
  const sendtogroup = async () => {
    const reg = new firebase();
    const result = await reg.sendToGroup(data);
    return result;
  };
  const response = await sendtogroup();
  console.log("response comand_hald22er", response);
  return response;
}; 

const sendMulticast = async (data) => {
  const sendmulticast = async () => {
    const reg = new firebase();
    const result = await reg.sendMulticast(data);
    return result;
  };
  const response = await sendmulticast();
  console.log("response comand_hald22er", response);
  return response;
}; 

module.exports = {
  sendToTopic: sendToTopic,
  sendToDevice:sendToDevice,
  sendToGroup:sendToGroup,
  sendMulticast:sendMulticast
};
