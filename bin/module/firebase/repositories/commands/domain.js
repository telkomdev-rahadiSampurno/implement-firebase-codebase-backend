
const wrapper = require('../../../../helpers/utils/wrapper');
const admin = require('firebase-admin');
const serviceAccount = require('../../utils/serviceAccount ');
const teks = require('../../../../infra/configs/teks'); 

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sharing-session-rahadi.firebaseio.com"
});

class firebase {

  async sendToDevice(data) {
    const registrasiToken = data.registrasiToken;
    const message = {
      'data': {
        'namaPromo': data.namaPromo,
        'hargaPromo': data.hargaPromo,
      },
      'notification': {
        'title': data.namaPromo,
        'body': data.hargaPromo
      }
    };
    const option = {
      'contentAvailable': true,
      'priority': 'high'
    };
    let response = await admin.messaging().sendToDevice(registrasiToken, message, option);
    if (response.failureCount > 0) {
      return wrapper.data(teks.WRAPPER.FAIL, teks.WRAPPER.ERROR, 400);
    }
    return wrapper.data(teks.WRAPPER.OK, teks.WRAPPER.SUCCESS_SEND_NOTIF, 201);
  }

  async sendToTopic(data) {
    const topic = data.topic;
    const message = {
      'data': {
        'namaPromo': data.namaPromo,
        'hargaPromo': data.hargaPromo
      },
      'notification': {
        'title': data.namaPromo,
        'body': data.hargaPromo
      }

    };
    const option = {
      'contentAvailable': true,
      'priority': 'high'
    };
    let response = await admin.messaging().sendToTopic(topic, message, option);
    if (response.failureCount > 0) {
      return wrapper.data(teks.WRAPPER.FAIL, teks.WRAPPER.ERROR, 400);
    }
    return wrapper.data(teks.WRAPPER.OK, teks.WRAPPER.SUCCESS_SEND_NOTIF, 201);
  }

  async sendToGroup(data) {
    const notificationKey = data.notificationKey;
    const payload = {
      'data': {
        'namaPromo': data.namaPromo,
        'hargaPromo': data.hargaPromo
      },
      'notification': {
        'title': data.namaPromo,
        'body': data.hargaPromo
      }

    };
    const option = {
      'contentAvailable': true,
      'priority': 'high'
    };

    let response = await admin.messaging().sendToDeviceGroup(notificationKey, payload, option);
    if (response.failureCount > 0) {
      return wrapper.data(teks.WRAPPER.FAIL, teks.WRAPPER.ERROR, 400);
    }
    return wrapper.data(teks.WRAPPER.OK, teks.WRAPPER.SUCCESS_SEND_NOTIF, 201);
  }

  async sendMulticast(data) {
    const registrationTokens = [
      'dZCe_I40Z0U:APA91bGlMrfN6v8jTkzamDreOGOxRXt9yzql8IZNIi_xZIy5_TpQvTgnKEPA7tPLmFcTQfLOTFZi8RkL1x7P7s5NFyUtMemLvqTi4FbycuLAIR55A-LPE8805pOITLLjWoe0isiUvKZP',
      'cF6bIOL8LgY:APA91bEU42XteYVuOqDkoSde3bI_1joOvsCtIdYuNnPJzZPQfilVHU-YGr2rR9lJtvhENW3980XflmeaAA9YlSVMz-Mnk0xoBqnWDUBmL5trCMvzhil8XeUwdYCRc_-RZMSi42vGsp0n',
    ];
    const message = {
      'data': {
        'namaPromo': data.namaPromo,
        'hargaPromo': data.hargaPromo
      },
      'notification': {
        'title': data.namaPromo,
        'body': data.hargaPromo
      },
      'tokens': registrationTokens,
    };
    try {
      let response = await admin.messaging().sendMulticast(message);
      if (response.failureCount > 0) {
        const failedTokens = [];
        response.responses.forEach((resp, idx) => {
          if (!resp.success) {
            failedTokens.push(registrationTokens[idx]);
          }
        });
        return wrapper.data(teks.WRAPPER.FAIL, failedTokens, 400);
      }
      return wrapper.data(teks.WRAPPER.OK, teks.WRAPPER.SUCCESS_SEND_NOTIF, 201);
    } catch (error) {
      return wrapper.data(teks.WRAPPER.FAIL, teks.WRAPPER.ERROR, 400);
    }
  }
}

module.exports = firebase;
