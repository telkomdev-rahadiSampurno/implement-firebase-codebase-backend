'use strict';
 
const WRAPPER = {
  FAIL: 'fail',
  ERROR: 'Error',
  SUCCESS_SEND_NOTIF: 'success send notification', 
  OK: 'ok',
}; 

module.exports = { 
  WRAPPER
};
