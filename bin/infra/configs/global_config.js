'use strict';
const nconf = require('nconf');

const DB_URL = () => {
    return nconf.get('DEVELOPMENT_MYSQL_DATABASE_CONFIG');
}

const getSentryDSN = () => {
    return nconf.get('DSN_SENTRY_URL');
}

const getImgUrlConvert = () => {
    return nconf.get('IMAGE_CONVERTER_URL');
}
 
  
module.exports = {
    DB_URL,
    getSentryDSN,
    getImgUrlConvert,

}