'use strict';

let userDatas = [
  { 'username': 'tmoney', 'password': 'ff866f5b6154bbb7d978e5243b49320b0d1ad67c' }
];

class User {
  constructor(username, password) {
    this.username = username;
    this.password = password;
  }
  isValidPassword(password) {
    return this.password === password;
  }
}

module.exports.findByUsername = function (username, cb) {
  let userData = '';
  for (let i = 0; i < userDatas.length; i++) {
    if (userDatas[i].username === username) {
      userData = userDatas[i];
      break;
    }

  }
  let user = new User(userData.username, userData.password);
  cb(user);
};