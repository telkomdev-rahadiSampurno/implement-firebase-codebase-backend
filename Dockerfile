FROM telkomindonesia/alpine:nodejs-8.9.3

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm i -g npm

RUN npm install

COPY . .

RUN mkdir uploads && chmod -R 777 uploads

EXPOSE 9000

User user

CMD ["npm", "start"]