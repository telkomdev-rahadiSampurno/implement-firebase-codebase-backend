const functions = require('firebase-functions');
const admin = require("firebase-admin");
const serviceAccount = require("./sharing-session-rahadi-firebase-adminsdk-q6auo-a30efa460a");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://sharing-session-rahadi.firebaseio.com"
});


exports.sendToDevice = functions.https.onRequest((req, res) => {
    const registrasiToken = req.query.registrasiToken;
    const payload = {
        'data': {
            'nama': req.query.nama,
            'kelas': req.query.kelas
        },
        'notification': {
            'title': req.query.title,
            'body': req.query.body
        }
    }

    const option = {
        'contentAvailable': true,
        'priority': 'high'
    }
    admin.messaging().sendToDevice(registrasiToken, payload, option).then((response) => {
        console.log('success');
        res.send({
            'success': true,
            'data': 'send notifications',
            'code': 200
        });
    });
}) 
